<?php
//creando variables
$nombre = "Ramon";
$poblacion = "Santander";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3</title>
</head>

<body>

    <?php
    //impresion en php
    echo "<div>";
    echo $nombre;
    echo "</div>";
    echo "<div>";
    echo $poblacion;
    echo "</div>";

    //impresion con concatenada
    echo "<div>" . $nombre . "</div>";
    echo "<div>" . $poblacion . "</div>";

    //otra solucion
    echo "<div> {$nombre} </div>";
    echo "<div> {$poblacion} </div>";

    //utilizando heredoc
    $salida = <<<"BLOQUE"
    <div> {$nombre} </div>
    <div> {$nombre} </div>
    BLOQUE;
    ?>

</body>

</html>